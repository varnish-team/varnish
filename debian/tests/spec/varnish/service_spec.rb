require 'spec_helper'

describe service('varnish') do
# Disable this check because ruby-specinfra is broken and only checks for
# the init script or the Upstart service.
#  it { should be_enabled }
  it { should be_running }
end

describe command('service varnish reload') do
  its(:exit_status) { should eq 0 }
  its(:stderr) { should eq('') }
end
