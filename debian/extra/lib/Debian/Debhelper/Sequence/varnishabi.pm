use warnings;
use strict;

use Debian::Debhelper::Dh_Lib;

# dh_varnishabi runs unconditionally, and before dh_gencontrol, so that
# the latter can use the substvars that are put into place by the former.
insert_before('dh_gencontrol', 'dh_varnishabi');

1;
